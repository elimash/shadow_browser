name := """SHADOW_BROWSER"""
organization := "com.shadow"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.2"

resolvers += "Central" at "https://repo1.maven.org/maven2/"
libraryDependencies += guice
// https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-java
libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "3.141.59"
// https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-htmlunit-driver
libraryDependencies += "org.seleniumhq.selenium" % "selenium-htmlunit-driver" % "2.52.0"
// https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-firefox-driver
libraryDependencies += "org.seleniumhq.selenium" % "selenium-firefox-driver" % "3.141.59"
// https://mvnrepository.com/artifact/org.freemarker/freemarker
libraryDependencies += "org.freemarker" % "freemarker" % "2.3.30"
libraryDependencies += "com.google.code.gson" % "gson" % "2.2.4"
libraryDependencies ++= Seq(
  javaWs
)





