#!/bin/bash
nohup ./sbt_runProd </dev/null >/tmp/shadow_browser.log 2>&1  &
echo "---------------- SHADOW-BROWSER Started and runs in background -----"
echo "---------------- Now: 'tail -f /tmp/shadow_browser.log'        -----"
echo "--------------------------------------------------------------------"
tail -f /tmp/shadow_browser.log