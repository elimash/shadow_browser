package freemarker;

        import java.util.List;

        import com.google.gson.Gson;

        public class SearchResultItem {
        private static final String SEARCH_ENGINE = "search_engine";
        public static final String _SHADOWPAGE = "shadowpage";
        public static final String AMAZON_CO = "amazon.co";
        public static final String _IMAGE_ITEM = "image_item";
        public static final String _PRODUCT_SEARCH = "product_search";
        public static final String _SEARCH_RESULT = "search_result";
        public static final String _NEWS_ITEM = "news_item";
        public static final String _RELATED_SEARCH = "related_search";
        public static final String _GREP = "grep";
        public static final String _EMPTY_STRING = "";
        private static final String _SLASH = "/";
        private static final String UTF_8 = "utf-8";

        public enum ResultItemType {
        NotSet(99),
        SHADOWPAGE(5),
        WIZERPAGE(5),
        VIDEO(8),
        NEWSITEM(7),
        IMAGE(9),
        WEBPAGE(10),
        RELATED_SEARCH(11),
        AD(13),
        GREP(14),
        CATEGORY(4),
        TAG (4),
        PRODUCT (2),
        PLACE_ID(6),
        COLLECTION(3),
        CAT_IN_PLACE (1),
        ORDER_BOX_PROMOTIONS(12),
        STANDARD_PROMOTION(13),
        DEAL(14),
        ARTICLE(4);

        private int priority;

        ResultItemType(int priority) { this.priority = priority; }

        public int getPriority() { return priority; }

        }


        private ResultItemType type;
        private String htmlSource;
        private String scriptSource; // For promotions
        private Integer promoPosition; // The promotion position
        private String title;
        private String category; // the category of a shadowpage (in case type=shadowpage) or the url-link
        private List<String> spCategories; // The categories in case of ShadowPage
        private String spUrlPath; // The urlPath of the ShadowPage
        private String author;
        private String time;
        private String video;
        private String[] text; // rolling text array
        private String description; // Additional data
        private String url;
        private String imgUrl;
        private List<Object> affiliateLinks;
        private Double score;
        private Integer scoreCount;
        private Integer bestDiscount;
        private String sourceDomain; // The domain or URL of this box/item source
        private String placeid; // In case of Google places, this is the place ID
        private String index; // This holds the index in case of a ShadowPage
        private String oid; // In case of a ShadowPage, this holds the OID
        private Integer sectionIndex; // Index into a sections vector when used in checkered list
        private Integer leagueRank; // In case the item is part of a league checkered, this will hold the position of the page in the league
        private Float bestSellingRank;
        private Object ribbon;
        private Boolean priceWatch; // If true, display price watch icon as the user has made a price watch for the page
        //private String suggestedDiscount;


        // Constructors
        public SearchResultItem(String resultTitle, String[] resultText, String url, String imgUrl, ResultItemType type) {
        this.title = resultTitle;
        this.text = resultText;
        this.url = url;
        this.imgUrl = imgUrl;
        this.type = type;
        this.category= type.name();
        }

        public ResultItemType getType() { return type; }

        public SearchResultItem setType(ResultItemType type) { this.type = type;return this; }

        public boolean isPromotion() {
        return ResultItemType.STANDARD_PROMOTION.equals(type) || ResultItemType.ORDER_BOX_PROMOTIONS.equals(type);
        }

        public boolean isAmazonProduct() {
        return ResultItemType.PRODUCT.equals(type) && sourceDomain != null && sourceDomain.toLowerCase().contains(AMAZON_CO);
        }

        public boolean isShadowPage() { return ResultItemType.SHADOWPAGE.equals(type); }

        public String getTitle() { return title; }
        public SearchResultItem setTitle(String title) { this.title = title; return this; }

    /*public String getSuggestedDiscount() { return suggestedDiscount; }
    public PASearchResultItem setSuggestedDiscount(String suggestedDiscount) { this.suggestedDiscount = suggestedDiscount; return this; }*/


        public Integer getBestDiscount() { return bestDiscount; }

        public SearchResultItem setBestDiscount(Integer bestDiscount) { this.bestDiscount = bestDiscount; return this; }


        public String[] getText() { return text; }

        public SearchResultItem setText(String[] text) { this.text = text; return this; }

        public boolean hasText() { return this.text != null && this.text.length > 0; }


        public SearchResultItem setVideo(String video) { this.video = video; return this; }

        public String getVideo() { return video; }


        public String getImgUrl() { return imgUrl; }

        public SearchResultItem setImgUrl(String imgUrl) { this.imgUrl = imgUrl;return this; }


        public String getCategory() { return category; }

        public SearchResultItem setCategory(String category) { this.category = category; return this; }

        public List<String> getShadowPageCategories() { return this.spCategories; }
        public SearchResultItem setShadowPageCategories(List<String> categories) { this.spCategories = categories; return this; }

        public String getShadowPageUrlPath() { return this.spUrlPath; }
        public SearchResultItem setShadowPageUrlPath(String urlPath) { this.spUrlPath = urlPath; return this; }

        public String getHtmlSource() { return htmlSource; }

        public SearchResultItem setHtmlSource(String htmlSource) { this.htmlSource = htmlSource; return this; }


        public Double getScore() { return score; }
        public SearchResultItem setScore(Double score) { this.score = score; return this; }

        public Integer getScoreCount() { return this.scoreCount; }
        public SearchResultItem setScoreCount(Integer scoreCount) { this.scoreCount = scoreCount; return this; }


        public String getDescription() { return description; }

        public SearchResultItem setDescription(String description) { this.description = description; return this; }


        public String getSourceDomain() { return sourceDomain; }

        public SearchResultItem setSourceDomain(String sourceDomain) { this.sourceDomain = sourceDomain; return this; }


        public String getPlaceid() { return placeid; }

        public SearchResultItem setPlaceid(String placeid) {this.placeid = placeid;	return this;}


        public String getUrl() { return url; }

        public SearchResultItem setUrl(String url) { this.url = url; return this; }


        public SearchResultItem setIndex(String index) { this.index = index; return this; }

        public String getIndex() { return this.index; }


        public SearchResultItem setOID(String oid) { this.oid = oid; return this; }

        public String getOid() { return this.oid; }


        public Integer getSectionIndex() { return sectionIndex; }

        public SearchResultItem setSectionIndex(int idx) { this.sectionIndex = idx; return this; }


        public Integer getLeagueRank() { return leagueRank; }

        public SearchResultItem setLeagueRank(Integer leagueRank) { this.leagueRank = leagueRank; return this; }

        public Float getBestSellingRank() { return bestSellingRank; }
        public SearchResultItem setBestSellingRank(Float bsr) { this.bestSellingRank = bsr; return this; }

        public SearchResultItem setAuthor(String author) { this.author = author; return this; }
        public SearchResultItem setTime(String time) { this.time = time; return this; }
        public String getTime() { return time; }
        public String getAuthor() { return author; }

        public SearchResultItem markPriceWatch() { this.priceWatch = true; return this; }
        public boolean isPriceWatch() { return this.priceWatch == null ? false : this.priceWatch; }
        public Integer getPromoPosition() { return this.promoPosition; }

        public String toString() { return new Gson().toJson(this); }

        }
