package freemarker;

import com.google.gson.Gson;
import controllers.datautils.searchdata.SearchResult;

import java.util.List;

public class ShadowBrowserData {
    String url;
    String pageImgBase64;
    List<ClickableElement> clickableElements;
    String dataAsJsonString;
    int pageWidth;
    int pageHeight;
    String searchQuery = null;
    List<SearchResult> searchResults = null;
    boolean showKeyboard = false;
    float keyboardTop;
    float keyboardLeft;
    float keyboardWidth;
    float inputTop;
    float inputLeft;
    float inputWidth;
    float inputHeight;
    String inputType;
    String keyboardBase64;
    String keyboardMap;


    // Constructors
    public ShadowBrowserData(int pageWidth, int pageHeight) {
        this.pageWidth = pageWidth;
        this.pageHeight = pageHeight;
    }

    // Methods
    public String toJsonString() {
        Gson gson = new Gson();
        this.dataAsJsonString = gson.toJson(this);
        return this.dataAsJsonString;
    }

    // Getters & Setters
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
        toJsonString();
    }

    public String getPageImgBase64() {
        return pageImgBase64;
    }
    public void setPageImgBase64(String pageImgBase64) {
        this.pageImgBase64 = pageImgBase64;
        toJsonString();
    }

    public List<ClickableElement> getClickableElements() {
        return clickableElements;
    }
    public void setClickableElements(List<ClickableElement> clickableElements) {
        this.clickableElements = clickableElements;
        toJsonString();
    }

    public String getDataAsJsonString() {  return dataAsJsonString;   }
    public void setDataAsJsonString(String dataAsJsonString) {
        this.dataAsJsonString = dataAsJsonString;
    }

    public int getPageWidth() { return pageWidth;   }
    public void setPageWidth(int pageWidth) { this.pageWidth = pageWidth;   }

    public int getPageHeight() { return pageHeight;  }
    public void setPageHeight(int pageHeight) { this.pageHeight = pageHeight;    }

    public String getSearchQuery() {   return searchQuery;    }
    public void setSearchQuery(String searchQuery) {   this.searchQuery = searchQuery;    }

    public List<SearchResult> getSearchResults() { return searchResults;    }
    public void setSearchResults(List<SearchResult> searchResults) {   this.searchResults = searchResults;    }

    public boolean getShowKeyboard() { return showKeyboard;}

    public void setShowKeyboard(boolean showKeyboard) { this.showKeyboard = showKeyboard;}

    public float getKeyboardTop() {return keyboardTop;}

    public void setKeyboardTop(float keyboardTop) { this.keyboardTop = keyboardTop; }

    public float getKeyboardLeft() { return keyboardLeft; }

    public void setKeyboardLeft(float keyboardLeft) {this.keyboardLeft = keyboardLeft;}

    public float getKeyboardWidth() {return keyboardWidth; }

    public void setKeyboardWidth(float keyboardWidth) {this.keyboardWidth = keyboardWidth;}

    public String getKeyboardBase64() {
        return keyboardBase64;
    }

    public void setKeyboardBase64(String keyboardBase64) {
        this.keyboardBase64 = keyboardBase64;
    }

    public String getKeyboardMap() {
        return keyboardMap;
    }

    public void setKeyboardMap(String keyboardMap) {
        this.keyboardMap = keyboardMap;
    }

    public float getInputTop() {
        return inputTop;
    }

    public void setInputTop(float inputTop) {
        this.inputTop = inputTop;
    }

    public float getInputLeft() {
        return inputLeft;
    }

    public void setInputLeft(float inputLeft) {
        this.inputLeft = inputLeft;
    }

    public float getInputWidth() {
        return inputWidth;
    }

    public void setInputWidth(float inputWidth) {
        this.inputWidth = inputWidth;
    }

    public float getInputHeight() {
        return inputHeight;
    }

    public void setInputHeight(float inputHeight) {
        this.inputHeight = inputHeight;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }
}