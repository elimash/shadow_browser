package freemarker;

import org.openqa.selenium.Rectangle;

public class ClickableElement {
    Rectangle rect;
    String url;
    String text;

    public ClickableElement(Rectangle rect, String url) {
        this.rect = rect;
        this.url = url;
    }

    public Rectangle getRect() {
        return rect;
    }

    public void setRect(Rectangle rect) {
        this.rect = rect;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
