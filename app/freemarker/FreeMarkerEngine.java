package freemarker;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import org.apache.commons.io.IOUtils;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.FileTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.core.Environment;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import static freemarker.template.Configuration.getVersion;

/**
 * We create 5 instances to fasten the rendering throughput.
 *
 * Created by elimashiah on 07/08/2017.
 */
public class FreeMarkerEngine {
    private static final String MY_NAME = FreeMarkerEngine.class.getSimpleName();

    private static final String TemplatesFolder = "/freemarker/templates";
    private static final int MAX_INSTANCES = 5;

    private static FreeMarkerEngine ourInstance = new FreeMarkerEngine();
    private static FreeMarkerEngine[] ourInstances = null;
    private static int currentInstance = 0;
    private static boolean initialized;
    private static boolean doFtlStackTrace = true;
    private static boolean doJavaStackTrace = true;

    enum FreeMarkerErrorReportingType {
        Html("Html", TemplateExceptionHandler.HTML_DEBUG_HANDLER),
        Debug("Debug", TemplateExceptionHandler.DEBUG_HANDLER),
        Ignore("Ignore", TemplateExceptionHandler.IGNORE_HANDLER),
        Shadow("Shadow", new ShadowLoggingHandler());

        FreeMarkerErrorReportingType(String name, TemplateExceptionHandler handler) {
            this.asString = name;
            this.handler = handler;
        }

        private TemplateExceptionHandler handler;
        private String asString;

        public static TemplateExceptionHandler getHandler(String name) {
            for(FreeMarkerErrorReportingType t : FreeMarkerErrorReportingType.values()) {
                if (name.equalsIgnoreCase(t.asString))
                    return t.handler;
            }
            return TemplateExceptionHandler.HTML_DEBUG_HANDLER; // If no match name found, assume it is a debugging/testing machine
        }
    }

    private Configuration cfg;
    private String templatesDir;
    private boolean printTraceInRender = true;

    // Coonstructor for multiple instances mode
    public FreeMarkerEngine() {
        FreeMarkerEngine.init(this);
    }

    private static class ShadowLoggingHandler implements TemplateExceptionHandler {
        @Override
        public void handleTemplateException (TemplateException te, Environment environment, Writer writer) throws TemplateException {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            pw.print(MY_NAME + ":: Template error caught by ShadowLoggingHandler:\n");
            te.printStackTrace(pw, false, doFtlStackTrace, doJavaStackTrace);
            pw.flush();
            sw.flush();
            System.err.println(sw.toString());
        }
    }

    public static FreeMarkerEngine getInstance() {
        if (ourInstances == null || !initialized ) {
            synchronized (FreeMarkerEngine.class) {
                if (!initialized) {
                    init();
                }
            }
        }
        currentInstance = (currentInstance + 1)%MAX_INSTANCES;
        return ourInstances[currentInstance];
    }

    private static void init() {
        ourInstances = new FreeMarkerEngine[MAX_INSTANCES];
        for (int i=0; i < ourInstances.length; i++) {
            ourInstances[i] = new FreeMarkerEngine();
        }
        initialized = true;
        return;
    }
    private static void init (FreeMarkerEngine instance) {

        String shadowSourceFolder = "public";
        if (instance == null) {
            instance = ourInstance; // in case of singelton
        }
        instance.cfg = new Configuration();
        try {
            instance.templatesDir = shadowSourceFolder + TemplatesFolder;

            FileTemplateLoader ftl1 = new FileTemplateLoader(new File(instance.templatesDir));
            ClassTemplateLoader ctl = new ClassTemplateLoader(instance.getClass(), instance.templatesDir);
            MultiTemplateLoader mtl = new MultiTemplateLoader(new TemplateLoader[] { ftl1, ctl });

            instance.cfg.setTemplateLoader(mtl);
            instance.cfg.setDefaultEncoding("UTF-8");

            // Figure out what should we do in case of errors
            TemplateExceptionHandler handler = TemplateExceptionHandler.HTML_DEBUG_HANDLER; // The default value, for vagrant, test machines and jenkins
            String handlerName = FreeMarkerErrorReportingType.Html.name();

            instance.cfg.setTemplateExceptionHandler(handler);
            instance.printTraceInRender = !(handler instanceof ShadowLoggingHandler);
        } catch (IOException e) {
            System.err.println("Failed to initialize FreeMarkerEngine exceptionMsg=" + e.getMessage());

        }
    }



    // Public Methods
    public Template getTemplate(String templateName) {
        try {
            return cfg.getTemplate(templateName);
        } catch (IOException e) {
            System.err.println("Failed to load FreeMarker Template=" + templateName + " msg=" + e.getMessage());
        }
        return null;
    }

    public String render(Template template, Object dataModel) {
        if (template == null) {
            System.err.println("FreeMarkerEngine can not process a null template");
            return null;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(baos);
        try {
            template.process(dataModel, osw);
        } catch (TemplateException e) {
            if (printTraceInRender)
                System.err.println("Failed to process template=" + template.getName() + " error.msg=" + e.getMessage() + " dataModel=" + dataModel.toString() );
            else
                System.err.println("Failed to process template=" + template.getName() + " error.msg=" + e.getMessage());
        } catch (IOException e) {
            System.err.println("Failed to process template IOException template=" + template.getName() + " error.msg=" + e.getMessage() + " dataModel=" + dataModel.toString() );
        }

        try {
            return baos.toString(cfg.getDefaultEncoding());
        } catch (UnsupportedEncodingException e) {
            System.err.println("Failed to produce finalHtml with encoding=" + cfg.getDefaultEncoding() + " error.msg=" + e.getMessage());
        }
        return null;
    }

    public String render(String templateName, Object dataModel) {
        Template template = this.getTemplate(templateName);
        if (template == null) {
            System.err.println("Failed to load template=" + templateName);
            return null;
        }
        return this.render(template, dataModel);
    }

    public static String staticRender(String templateName, Object dataModel) {
        FreeMarkerEngine sing = FreeMarkerEngine.getInstance();
        Template template = sing.getTemplate(templateName);
        if (template == null) {
            System.err.println("Failed to load template=" + templateName);
            return null;
        }
        return sing.render(template, dataModel);
    }

    // Getters & Setters
    public Configuration getCfg() { return cfg; }
}
