package controllers.datautils.searchdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Title {

    @SerializedName("iconUrl")
    @Expose
    private String iconUrl;
    @SerializedName("iconHref")
    @Expose
    private String iconHref;

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getIconHref() {
        return iconHref;
    }

    public void setIconHref(String iconHref) {
        this.iconHref = iconHref;
    }

}