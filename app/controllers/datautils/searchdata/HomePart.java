package controllers.datautils.searchdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomePart {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("href")
    @Expose
    private String href;
    @SerializedName("categoryFQCN")
    @Expose
    private String categoryFQCN;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getCategoryFQCN() {
        return categoryFQCN;
    }

    public void setCategoryFQCN(String categoryFQCN) {
        this.categoryFQCN = categoryFQCN;
    }

}