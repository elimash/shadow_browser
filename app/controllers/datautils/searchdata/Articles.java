package controllers.datautils.searchdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Articles {

    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("articlesTOC_FN")
    @Expose
    private String articlesTOCFN;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getArticlesTOCFN() {
        return articlesTOCFN;
    }

    public void setArticlesTOCFN(String articlesTOCFN) {
        this.articlesTOCFN = articlesTOCFN;
    }

}