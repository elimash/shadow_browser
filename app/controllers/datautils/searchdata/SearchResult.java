package controllers.datautils.searchdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchResult {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("text")
    @Expose
    private List<String> text = null;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("imgUrl")
    @Expose
    private String imgUrl;
    @SerializedName("affiliateLinks")
    @Expose
    private List<AffiliateLink> affiliateLinks = null;
    @SerializedName("score")
    @Expose
    private Double score;
    @SerializedName("sourceDomain")
    @Expose
    private String sourceDomain;
    @SerializedName("promoPosition")
    @Expose
    private Integer promoPosition;
    @SerializedName("ribbon")
    @Expose
    private Ribbon ribbon;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getText() {
        return text;
    }

    public void setText(List<String> text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public List<AffiliateLink> getAffiliateLinks() {
        return affiliateLinks;
    }

    public void setAffiliateLinks(List<AffiliateLink> affiliateLinks) {
        this.affiliateLinks = affiliateLinks;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getSourceDomain() {
        return sourceDomain;
    }

    public void setSourceDomain(String sourceDomain) {
        this.sourceDomain = sourceDomain;
    }

    public Integer getPromoPosition() {
        return promoPosition;
    }

    public void setPromoPosition(Integer promoPosition) {
        this.promoPosition = promoPosition;
    }

    public Ribbon getRibbon() {
        return ribbon;
    }

    public void setRibbon(Ribbon ribbon) {
        this.ribbon = ribbon;
    }

}