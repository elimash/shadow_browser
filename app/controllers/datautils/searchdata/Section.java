package controllers.datautils.searchdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Section {

    @SerializedName("pages")
    @Expose
    private Pages pages;
    @SerializedName("friendlyName")
    @Expose
    private String friendlyName;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("href")
    @Expose
    private String href;
    @SerializedName("hasMore")
    @Expose
    private Boolean hasMore;
    @SerializedName("sectionType")
    @Expose
    private String sectionType;
    @SerializedName("articles")
    @Expose
    private Articles articles;

    public Pages getPages() {
        return pages;
    }

    public void setPages(Pages pages) {
        this.pages = pages;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public Boolean getHasMore() {
        return hasMore;
    }

    public void setHasMore(Boolean hasMore) {
        this.hasMore = hasMore;
    }

    public String getSectionType() {
        return sectionType;
    }

    public void setSectionType(String sectionType) {
        this.sectionType = sectionType;
    }

    public Articles getArticles() {
        return articles;
    }

    public void setArticles(Articles articles) {
        this.articles = articles;
    }

}