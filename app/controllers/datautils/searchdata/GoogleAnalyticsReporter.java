package controllers.datautils.searchdata;

import controllers.datautils.WebFetcher;
import play.mvc.Http;

import java.io.IOException;

public class GoogleAnalyticsReporter
{
    static GoogleAnalyticsReporter instance = null;
    static WebFetcher fetcher = null;
    static final String baseEndPoint = "https://www.google-analytics.com/collect?v=1";
    static final String defaultTID = "UA-45283899-1";
    String tid; // Property-id UA-XXXX

    private  GoogleAnalyticsReporter(String tid) {
            this.tid = tid;
            fetcher = new WebFetcher();
            instance = this;
    }
    public static GoogleAnalyticsReporter getInstance() {
        if (instance == null) return getInstance(defaultTID);
        return instance;
    }
    public static GoogleAnalyticsReporter getInstance(String tid) {
        if (instance != null) return instance;
        synchronized (GoogleAnalyticsReporter.class) {
            if (instance == null) {
                    instance = new GoogleAnalyticsReporter(tid);
                }
            }
        return instance;
    }

    public void sessionStart(Http.Request request, String sessionID)  {
        send2GA(request, "&sc=start&cid=" + sessionID);
    }
    public void sessionEnd(Http.Request request, String sessionID) {
        send2GA(request, "&sc=end&cid=" + sessionID);; // Session-Control = end
    }
    public void reportPageview(Http.Request request, String sessionID) {
        send2GA(request, "&cid=" + sessionID + "&t=pageview" + "&dl=" + request.uri() );
    }
    public void reportEvent(Http.Request request, String sessionID, GAEvent event) {
        String eventParams = "";
        if (event != null) {
            eventParams = event.toGAString();
        };
        send2GA(request, "&cid=" + sessionID + "&t=event"  + "&dl=" + WebFetcher.encodeURL(request.uri()) + eventParams );
    }

    private void send2GA(Http.Request request, String params) {
        String endpoint = baseEndPoint + "&tid=" + this.tid;
        String useragent = null;
        if (request != null) {
            useragent = request.getHeaders().get("User-Agent").get();
            String ip = request.remoteAddress();
            endpoint += "&uip=" + ip;
            String host = request.host();
            if (host.startsWith("localhost")) {
                host = "shadow.com";
            }
            endpoint += "&dh=" + host;
            endpoint += "&dt=" + "Shadow%20Browser";
        }
        endpoint += params;
        try {
            fetcher.get(endpoint, useragent);
        } catch (IOException e) {
            System.out.println("Failed to report GA about:" + endpoint);
        }
    }

    public static class GAEvent {
        String eventCategory;
        String eventAction;
        String eventLabel;
        String eventValue;
        public GAEvent(String category, String action, String label, String value) {
            this.eventCategory = category;
            this.eventAction = action;
            this.eventLabel = label;
            this.eventValue = value;
        }
        private String toGAString() {
            String eventParams = null;
            eventParams = "";
            if (eventCategory != null) {
                eventParams += "&ec=" + WebFetcher.encodeURL(eventCategory);
            }
            if (eventAction != null) {
                eventParams += "&ea=" + WebFetcher.encodeURL(eventAction);
            }
            if (eventLabel != null) {
                eventParams += "&el=" + WebFetcher.encodeURL(eventLabel);
            }
            if (eventValue != null) {
                eventParams += "&ev=" + eventValue;
            }
            return eventParams;
        }
    }
}
