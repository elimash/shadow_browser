package controllers.datautils.searchdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pages {

    @SerializedName("sourceName")
    @Expose
    private String sourceName;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("results")
    @Expose
    private List<SearchResult> results = null;
    @SerializedName("isCollection")
    @Expose
    private Boolean isCollection;
    @SerializedName("failure")
    @Expose
    private Boolean failure;

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<SearchResult> getResults() {
        return results;
    }

    public void setResults(List<SearchResult> results) {
        this.results = results;
    }

    public Boolean getIsCollection() {
        return isCollection;
    }

    public void setIsCollection(Boolean isCollection) {
        this.isCollection = isCollection;
    }

    public Boolean getFailure() {
        return failure;
    }

    public void setFailure(Boolean failure) {
        this.failure = failure;
    }

}