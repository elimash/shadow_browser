package controllers.datautils.searchdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BreadCrumbs {

    @SerializedName("homePart")
    @Expose
    private HomePart homePart;
    @SerializedName("isCaptive")
    @Expose
    private Boolean isCaptive;
    @SerializedName("metaTitle")
    @Expose
    private String metaTitle;
    @SerializedName("metDescription")
    @Expose
    private String metDescription;
    @SerializedName("tags")
    @Expose
    private List<Tag> tags = null;

    public HomePart getHomePart() {
        return homePart;
    }

    public void setHomePart(HomePart homePart) {
        this.homePart = homePart;
    }

    public Boolean getIsCaptive() {
        return isCaptive;
    }

    public void setIsCaptive(Boolean isCaptive) {
        this.isCaptive = isCaptive;
    }

    public String getMetaTitle() {
        return metaTitle;
    }

    public void setMetaTitle(String metaTitle) {
        this.metaTitle = metaTitle;
    }

    public String getMetDescription() {
        return metDescription;
    }

    public void setMetDescription(String metDescription) {
        this.metDescription = metDescription;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

}