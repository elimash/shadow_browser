package controllers.datautils.searchdata;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchResponse {

    @SerializedName("result_status")
    @Expose
    private String resultStatus;
    @SerializedName("reply_version")
    @Expose
    private String replyVersion;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("title")
    @Expose
    private Title title;
    @SerializedName("searchOpts")
    @Expose
    private SearchOpts searchOpts;
    @SerializedName("breadCrumbs")
    @Expose
    private BreadCrumbs breadCrumbs;
    @SerializedName("sections")
    @Expose
    private List<Section> sections = null;
    @SerializedName("adultIntent")
    @Expose
    private Boolean adultIntent;
    @SerializedName("catMetaTitle")
    @Expose
    private String catMetaTitle;
    @SerializedName("catMetaDescription")
    @Expose
    private String catMetaDescription;

    public String getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(String resultStatus) {
        this.resultStatus = resultStatus;
    }

    public String getReplyVersion() {
        return replyVersion;
    }

    public void setReplyVersion(String replyVersion) {
        this.replyVersion = replyVersion;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public SearchOpts getSearchOpts() {
        return searchOpts;
    }

    public void setSearchOpts(SearchOpts searchOpts) {
        this.searchOpts = searchOpts;
    }

    public BreadCrumbs getBreadCrumbs() {
        return breadCrumbs;
    }

    public void setBreadCrumbs(BreadCrumbs breadCrumbs) {
        this.breadCrumbs = breadCrumbs;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public Boolean getAdultIntent() {
        return adultIntent;
    }

    public void setAdultIntent(Boolean adultIntent) {
        this.adultIntent = adultIntent;
    }

    public String getCatMetaTitle() {
        return catMetaTitle;
    }

    public void setCatMetaTitle(String catMetaTitle) {
        this.catMetaTitle = catMetaTitle;
    }

    public String getCatMetaDescription() {
        return catMetaDescription;
    }

    public void setCatMetaDescription(String catMetaDescription) {
        this.catMetaDescription = catMetaDescription;
    }

}