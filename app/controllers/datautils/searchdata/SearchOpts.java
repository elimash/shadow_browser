package controllers.datautils.searchdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchOpts {

    @SerializedName("isGeo")
    @Expose
    private Boolean isGeo;

    public Boolean getIsGeo() {
        return isGeo;
    }

    public void setIsGeo(Boolean isGeo) {
        this.isGeo = isGeo;
    }

}