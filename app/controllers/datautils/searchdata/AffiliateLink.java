package controllers.datautils.searchdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AffiliateLink {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("displayUrl")
    @Expose
    private String displayUrl;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("domain")
    @Expose
    private String domain;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("c2covalue")
    @Expose
    private Integer c2covalue;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDisplayUrl() {
        return displayUrl;
    }

    public void setDisplayUrl(String displayUrl) {
        this.displayUrl = displayUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getC2covalue() {
        return c2covalue;
    }

    public void setC2covalue(Integer c2covalue) {
        this.c2covalue = c2covalue;
    }

}