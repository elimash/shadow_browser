package controllers.datautils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

/**
 * A trivial WebFetcher to bring information from other HTTP servers.
 * This simple webFetcher is designed for simple usages of webFetching (without authentication etc).
 *
 * It was copied mainly to use for Search functionality &  GA reporting.
 * I added the get with User-Agent String method.
 *
 * Copied from Shadow's UTWebFetcher by Eli Mashiah on 31/8/2020
 */
public class WebFetcher {

    public static final String DEFAULT_CHARACTER_ENCODING = "UTF-8";
    private final HttpClient client;
    private String characterEncoding;
    private static int defaultInacticityPeriod = 3000;

    public WebFetcher() { this(null); }
    public WebFetcher(String characterEncoding) {
        this.characterEncoding = characterEncoding;
        if (this.characterEncoding == null) {
            this.characterEncoding = DEFAULT_CHARACTER_ENCODING;
        }
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(20);
        cm.setDefaultMaxPerRoute(10);
        cm.setValidateAfterInactivity(defaultInacticityPeriod);
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(defaultInacticityPeriod).build();
        HttpClientBuilder builder = HttpClients.custom().setConnectionManager(cm).setDefaultRequestConfig(requestConfig);
        client = builder.build();
    }
    // getters & setters
    public String getCharacterEncoding() {
        return characterEncoding;
    }
    public static int getDefaultInacticityPeriod() { return defaultInacticityPeriod;	}
    public static void setDefaultInacticityPeriod(int period) {
        defaultInacticityPeriod = period;
    }

    public InputStream getInputStream(String uri) throws IOException {
        try {
            HttpGet get = new HttpGet(uri);
            return client.execute(get).getEntity().getContent();
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    private String readString(HttpResponse response) throws IOException {
        try (InputStream responseContent = response.getEntity().getContent()) {
            String str = IOUtils.toString(responseContent, characterEncoding);
            if (str == null || str.trim().length() == 0) {
                return null;
            }
            return str.trim();
        }
    }

    public String get(String uri) throws IOException {
        try {
            HttpGet get = new HttpGet(uri);
            return readString(client.execute(get));
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    public String get(String uri, String userAgent) throws IOException {
        try {
            HttpGet get = new HttpGet(uri);
            get.setHeader("User-Agent", userAgent);
            return readString(client.execute(get));
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    public String get(HttpGet request) throws IOException {
        String retStr;
        try {
            retStr = readString( client.execute(request) );
        } catch (Exception e) {
            throw new IOException(e);
        }
        return retStr;
    }

    /**
     * This sendAsync method is special implementation for Google-Analytics reporting
     *
     * @param uri
     * @param userAgent
     */
    public void sendAsyncGet(String uri, String userAgent) throws IOException {
        HttpGet get = new HttpGet(uri);
        if (userAgent != null) {
            get.setHeader("User-Agent", userAgent);
        }
        client.execute(get);
    }

    public String post(HttpPost data) throws IOException {
        try {
            return readString(client.execute(data));
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    public Header[] getHeaders(String uri, String header) throws IOException {
        HttpGet get = new HttpGet(uri);
        RequestConfig config = RequestConfig.custom().setRedirectsEnabled(false).build();
        get.setConfig(config);
        HttpResponse htr = null;
        Header[] hs = null;
        try {
            htr = client.execute(get);
            hs = htr.getHeaders(header);
        } finally {
            if (htr != null) {
                htr.getEntity().getContent().close();
            }
        }
        return hs;
    }

    public int getStatus(String url) throws IOException {
        HttpGet get = new HttpGet(url);
        RequestConfig config = RequestConfig.custom().setRedirectsEnabled(false).build();
        get.setConfig(config);
        HttpResponse htr = null;
        try {
            htr = client.execute(get);
        } finally {
            if (htr != null) {
                htr.getEntity().getContent().close();
            }
        }
        int statusCode = -1;
        if (htr != null && htr.getStatusLine() != null) {
            statusCode = htr.getStatusLine().getStatusCode();
        }
        return statusCode;
    }

    public static String encodeURL(String s) {
        String retStr = s;
        try {
            retStr = URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.out.println("Failed to encode s=" + s);
        }
        return retStr;
    }
}
