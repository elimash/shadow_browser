package controllers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import play.Logger;
import play.api.inject.ApplicationLifecycle;
import play.libs.F;
import scala.concurrent.impl.Promise;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletionStage;

@Singleton
public class BackgroundTasks {

    @Inject
    public BackgroundTasks(ApplicationLifecycle lifecycle) {
        lifecycle.addStopHook((Callable<? extends CompletionStage<?>>) () -> {
            System.out.println("onStop started");
            return (CompletionStage<?>) null;
        });
    }
}
